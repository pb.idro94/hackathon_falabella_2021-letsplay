# Hackathon Falabella 2021 let's play
## Team: Astroboys
### Members:
<ul>
<li>Roy Barrera</li>
<li>Sebastian Marchi</li>
<li>Pablo Bustamante</li>
</ul>
<br>

### How to read
The main file that contains all data of algotithm that train the recommendation sistem is <br>
https://gitlab.com/pb.idro94/hackathon_falabella_2021-letsplay/-/blob/main/cart_recommender.ipynb
<br>
there you will encounter the model trainning and examples of recurrent users and cold start users.<br>

to run the model is needed download de data.csv that is a pre cleaned version of given csv. <br><br>

This file was uploaded in google drive at this link <br>
https://drive.google.com/file/d/1K5Msh9OXIA1Td7E_91j8cIeaV2K23jR1/view?usp=sharing


### About Model and Metric
The Model choiced for training was lightfm <br>
https://making.lyst.com/lightfm/docs/home.html <br>

About Metric used to eval the model we used precision at k, wich is the fraction of known positives in the <br> first k positions of the ranked list of results. A perfect score is 1.0.
<br><b> we obtain a mean score of 0.043 wich is so good </b> considering the given time and clients behavior in the data. (only few clients buy recurrently)

### About Automation
How we think to put this models inside webapp backend we dont have plans to automate the output of the model.<br>
But the training can be automated if we accomulate more data using the interface. In that case the best way to<br> doit is using Airflow, Bigquery, Kubernets, Dockers and Python :) <br>

### About Api/Interface
This wasnt finished yet but can find the flask code in the file app.<ip> But you can see the model working in Jupyter. To Runit you must install the requirement.txt library. But is still no finalized the development.

### 2 Minutes Video
The video was uploaded Here -> https://falabella-my.sharepoint.com/personal/rabarrerar_sodimac_cl/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Frabarrerar%5Fsodimac%5Fcl%2FDocuments%2FArchivos%20de%20chat%20de%20Microsoft%20Teams%2FAstroboysv2%2Emp4&parent=%2Fpersonal%2Frabarrerar%5Fsodimac%5Fcl%2FDocuments%2FArchivos%20de%20chat%20de%20Microsoft%20Teams&ct=1637206636627&or=Teams%2DHL

<br>

### Presentation Power Point
The presentation is located Here <br>
https://gitlab.com/pb.idro94/hackathon_falabella_2021-letsplay/-/blob/main/Hackatton2021_Astroboys.pptx <br>


### State of development
<img src="https://gitlab.com/pb.idro94/hackathon_falabella_2021-letsplay/-/raw/main/devgrade.png">




 
